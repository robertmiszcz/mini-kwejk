package com.example.kwejk.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GifDaoImpl implements GifDao {



   private static  List<Category> categories = new ArrayList<>();

   static{
        categories.add(new Category("Android",1));
        categories.add(new Category("Funny",2));
        categories.add(new Category("Programming",3));
    }

    private static List<Gif> names = new ArrayList<>();

    static {
        names.add(new Gif("android-explosion","Sitek1989",true, categories.get(0)));
        names.add(new Gif("ben-and-mike", "ArturZorro",false, categories.get(1)));
        names.add(new Gif("book-dominos","Sratatatata",false, categories.get(2)));
        names.add(new Gif("compiler-bot","Stasiek",true, categories.get(2)));
        names.add(new Gif("cowboy-coder","DupaJasio",true, categories.get(2)));
        names.add(new Gif("infinite-andrew","MalinowyPierdziel",false, categories.get(1)));
    }

    public List<Gif> findAll() {
        List<Gif> gifs = new ArrayList<>();
        int i = 1;
        for (Gif name : names) {
            gifs.add(name);
        }
        return gifs;
    }

    public List<Gif> findFavorites() {
        List<Gif> favorites = new ArrayList<>();
        int i = 1;
        for (Gif name : names) {
            if(name.isFavorite()==true)
            favorites.add(name);
        }
        return favorites;
    }

    public Gif findOne(String name){
        return findAll().stream().filter((a)->a.getName().equals(name)).collect(Collectors.toList()).get(0);
    }

    public List<Category> showCategories(){

        return categories;

    }


    public Category findCategoryById (int id) {

        for (Category category : categories) {
            if (category.getId() == id)
                return category;
        }
        return null;

    }


    public List<Gif> findByCategory(int id) {
        List<Gif> oneCategory = new ArrayList<>();
        for (Gif n : names) {
            if (n.category.getId()== id)
                oneCategory.add(n);
        }
        return oneCategory;
      }

    public List<Gif> getSearchByName(String name){
        List<Gif> searchAll = new ArrayList<>();

        for(Gif n: names){
            if(n.getName().contains(name.toLowerCase()) || n.getCategory().getName().toLowerCase().equals(name.toLowerCase())){
                searchAll.add(n);
            }
        }

        return searchAll;
    }

}



